import React from 'react';
import {Dimensions, Alert, StyleSheet, ActivityIndicator, TouchableHighlight, Text, TouchableOpacity, Button, View } from 'react-native';
import {RNCamera} from 'react-native-camera';
import CaptureButton from './CaptureButton.js';
import Clarifai from 'clarifai';

export default class Camera extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      identifiedAs: '',
      loading: false,
    };
  }

  render() {
    return (
        <RNCamera ref={ref => {this.camera = ref;}} style={styles.preview}>
            <ActivityIndicator size="large" style={styles.loadingIndicator} color="#fff" animating={this.state.loading}/>
            <CaptureButton buttonDisabled={this.state.loading} onClick={this.takePicture.bind(this)}/>
        </RNCamera>
    );
  }

  takePicture = async () => {
    if (this.camera) {
      // pause the camera's preview
      // this.camera.pauseReview();

      // Set the activity indicator
      this.setState((previousState, props) => ({
        loading: true,
      }));

      // Set options
      const options = {
        base64: true,
        quality: 0.5
      };

      // get the base64 version of the image
      const data = await this.camera.takePictureAsync(options);

      // Get the identified image
      this.identifyImage(data.base64);
    }
  };

  identifyImage(imageData) {
    // initialize the clarifai api
    const app = new Clarifai.App({
        apiKey: 'f8bfbe3e45dd478d9a4a9f85a9344f2d'
    });

    // identify the image
    app.models.predict(Clarifai.GENERAL_MODEL, {base64: imageData})
        .then((response) => {
            //console.log(response);
            this.displayAnswer(response.outputs[0].data.concepts[0].name)
        })
        .catch((err) => console.log(err))
  }

  displayAnswer(identifiedImage) {
      
    // Dismiss the activity indicator
    this.setState((prevState, props) => ({
        identifiedAs: identifiedImage,
        loading: false
    }));

    // Show an alert with the answer on
    Alert.alert(this.state.identifiedAs);

    // Resume the preview
    this.camera.resumePreview();
  }
}

const styles = StyleSheet.create({
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
  loadingIndicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
