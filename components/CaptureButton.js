import React from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';

export default class CaptureButton extends React.Component {
	render() {
		return (
			<TouchableOpacity
                style={styles.captureButton}
				disabled={this.props.buttonDisabled}
				onPress={this.props.onClick}>
				<Text style={{color: 'red', fontSize: 16}}>Capture</Text>
			</TouchableOpacity>
		);
	}
}

const styles = StyleSheet.create({
	captureButton: {
		marginBottom:30,
		width:160,
		borderRadius:10,
		backgroundColor: 'white',
	}
});